package com.makersf.games.nyanfly.perks;

import com.makersf.games.nyanfly.cavern.Point;

public class Light extends Point {

	public float intensity;

	public Light(float pX, float pY, float pIntensity) {
		super(pX, pY);
		intensity = pIntensity;
	}

	public void setIntensity(float pIntensity) {
		intensity = pIntensity;
	}

	@Override
	public int hashCode() {
		int x = Float.floatToIntBits(this.x);
		int y = Float.floatToIntBits(this.y);
		int i = Float.floatToIntBits(intensity);
		return 	(0xff00 & x << 16 | 0x00ff & x >> 16) ^
				(0xff00 & y << 16 | 0x00ff & y >> 16) ^
				(0xff00 & i << 16 | 0x00ff & i >> 16);
	}

	@Override
	public boolean equals(Object e) {
		if (e instanceof Light) {
			Light l = (Light) e;
			return l.x == this.x && l.y == this.y && l.intensity == this.intensity;
		}
		return false;
	}
}
