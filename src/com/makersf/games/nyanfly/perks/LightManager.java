package com.makersf.games.nyanfly.perks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import com.makersf.games.nyanfly.cavern.CavernProvider;
import com.makersf.games.nyanfly.cavern.IPlayerInformationsProvider;

public class LightManager {

	private static final float SECONDS_PER_MILLISECOND = 0.001f;
	private static final float MAX_UPDATE_STEP = 78 * SECONDS_PER_MILLISECOND;

	private static final Random RANDOM = new Random();

	private static final float MIN_LIGHT_INTENSITY = 0.02f;
	private static final float MAX_LIGHT_INTENSITY = 0.17f;
	private static final float LEFT_LIMIT_TO_ERASE_LIGHT = -MAX_LIGHT_INTENSITY;

	private static final float MIN_DISTANCE_BETWEEN_LIGHTS = 2 * MIN_LIGHT_INTENSITY;

	private int mMaxLights;
	private float mRightmostLight;
	private float mSpaceToNextLight;

	private final List<Light> mLights;
	private final List<Light> mUnmodifiableLights;
	private final CavernProvider mCavernProvider;
	private final IPlayerInformationsProvider mPlayer;

	public LightManager(int pMaxLights, CavernProvider pCavernProvider, IPlayerInformationsProvider pMovingBody) {
		mMaxLights = pMaxLights;
		mLights = new ArrayList<Light>(mMaxLights);
		mUnmodifiableLights = Collections.unmodifiableList(mLights);
		mCavernProvider = pCavernProvider;
		mPlayer= pMovingBody;
	}

	public int getMaxLights() {
		return mMaxLights;
	}

	private void shiftAll(float pX, float pY) {
		mRightmostLight += pX;
		ListIterator<Light> it = mLights.listIterator();
		while(it.hasNext()) {
			Light c = it.next();
			c.offset(pX, pY);
			if(c.x <= LEFT_LIMIT_TO_ERASE_LIGHT || c.y <= 0 || c.y >= 1) {
				it.remove();
			}
		}
		if(mLights.isEmpty()) {
			mRightmostLight = 0;
		}
	}

	public void reset() {
		mLights.clear();
		mRightmostLight = 0;
		populateFirstRound();
		mSpaceToNextLight = spaceToWaitForNextLight();
	}

	public synchronized void onUpdate(float pSecondsElapsed) {
		/*
		 * Forces the stepUpdate function to be called with at most MAX_UPDATE_STEP as parameter.
		 * This prevents the errors during debugging in which the player would travel almost a full screen in a single update
		 * raising errors, and making debugging miserable
		 */
		float deltaTime = pSecondsElapsed;
		while(deltaTime > 0) {
			float stepTime = Math.min(deltaTime, MAX_UPDATE_STEP);
			stepUpdate(stepTime);
			deltaTime -= stepTime;
		}
	}

	public void stepUpdate(float pSecondsElapsed) {
		float coveredSpace = mPlayer.getPercentOfScreenTraveledPerSecond() * pSecondsElapsed;
		mSpaceToNextLight -= coveredSpace;
		shiftAll(-coveredSpace, 0);
		if(mLights.size() < mMaxLights && mSpaceToNextLight <= 0) {
			// we add max_light_intensity so that half a circle doesn't appear out of nothing near the right of the screen
			float minX = Math.max(mRightmostLight, 1.f) + MAX_LIGHT_INTENSITY;
			float maxX = Math.min(mCavernProvider.maxSampleable(true), mCavernProvider.maxSampleable(false));
			if(minX > maxX) {
				//something's off
				//System.out.println("There is no way to generate a light in the range [" + minX + "," + maxX + "].");
				//throw new IllegalStateException("There is no way to generate a light in the range [" + minX + "," + maxX + "].");
				return;
			} else {
				float newX = minX + RANDOM.nextFloat() * (maxX - minX);
				addLight(generateLight(newX));
				mSpaceToNextLight = spaceToWaitForNextLight();
			}
		}
	}

	private void populateFirstRound() {
		float currentX = spaceToWaitForNextLight();
		while(currentX < 1.f && mLights.size() < mMaxLights) {
			generateLight(currentX);
			currentX += spaceToWaitForNextLight();
		}
	}

	private float spaceToWaitForNextLight() {
		float minDistance = MIN_DISTANCE_BETWEEN_LIGHTS;
		float maxDistance = 1.f / (mMaxLights - 1);
		if(minDistance > maxDistance) {
			throw new IllegalStateException("The maximum number of lights is too high: it's not possible to keep " + minDistance + " units between the lights.");
		}
		return minDistance + RANDOM.nextFloat() * (maxDistance - minDistance);
	}

	private Light generateLight(float pNewX) {
		float lowerBound = mCavernProvider.sample(pNewX, false) + 0.01f;
		float upperBound = mCavernProvider.sample(pNewX, true) - 0.01f;
		float newY = lowerBound + RANDOM.nextFloat() * (upperBound - lowerBound);
		float intensity = MIN_LIGHT_INTENSITY + RANDOM.nextFloat() * (MAX_LIGHT_INTENSITY - MIN_LIGHT_INTENSITY);
		return new Light(pNewX, newY, intensity);
	}

	private void addLight(Light pLight) {
		mLights.add(pLight);
		mRightmostLight = Math.max(mRightmostLight, pLight.x);
	}

	public void removeLight(Light pLight) {
		// We first get the index because pLight might be a different object, but equal to the one to remove.
		// We want to set the X of our internal object, not of the one used to specify what to remove!
		int index = mLights.indexOf(pLight);
		mLights.get(index).setX(Float.NEGATIVE_INFINITY);
		// This handles the cleanup
		shiftAll(0, 0);
	}

	public List<Light> getLightInformations() {
		return mUnmodifiableLights;
	}
}
