package com.makersf.games.nyanfly.cavern.cave;

public interface Sampleable {
	public float sample(float pX);
}
