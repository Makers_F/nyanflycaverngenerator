package com.makersf.games.nyanfly.cavern.cave;

import it.francesco.utils.math.Interpolation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.makersf.games.nyanfly.cavern.Point;
import com.makersf.games.nyanfly.cavern.Point.PointComparator;

public class CaveRappresentation implements Sampleable {
	private static final PointComparator comparator = new Point.PointComparator();

	private final int ARRAY_LENGHT;

	private final Point[] mPoints;
	private final List<Point> mUnmodifiablePointList;
	private final Point mDefaultPoint;
	private int mFirstFree;
	private float mRightmostX;

	public CaveRappresentation(int pNumberOfPoints, Point pDefaultPosition) {
		ARRAY_LENGHT = pNumberOfPoints;
		mPoints = new Point[ARRAY_LENGHT];
		mUnmodifiablePointList = Collections.unmodifiableList(Arrays.asList(mPoints));
		mDefaultPoint = new Point().set(pDefaultPosition);
		clear();
	}

	public void clear() {
		for(int i = 0; i < ARRAY_LENGHT; i++)
			mPoints[i] = new Point().set(mDefaultPoint);
		mFirstFree = 0;
		mRightmostX = 0;
	}

	public void offset(float pXOffset, float pYOffset) {
		for(int i = 0; i < mFirstFree; i++) {
			mPoints[i].offset(pXOffset, pYOffset);
		}
		mRightmostX += pXOffset;
	}

	public void shiftLeft() {
		for(int i = 0; i < ARRAY_LENGHT - 1; i++)
			mPoints[i].set(mPoints[i+1]);
		mPoints[ARRAY_LENGHT-1].set(mDefaultPoint);
		mFirstFree--;
		if(mFirstFree == 0)
			mRightmostX = 0;
	}

	public void addPoint(Point pPoint) {
		mPoints[mFirstFree].set(pPoint);
		mFirstFree++;
		mRightmostX = Math.max(pPoint.x, mRightmostX);
		Arrays.sort(mPoints, 0, mFirstFree, comparator);
	}

	public void setPoint(int pIndex, Point pPoint) {
		if (pIndex >= mFirstFree)
			throw new ArrayIndexOutOfBoundsException(pIndex);
		mPoints[pIndex].set(pPoint);
	}
	public boolean hasUnusedPoints() {
		return mFirstFree < ARRAY_LENGHT;
	}

	public int unusedPoints() {
		return ARRAY_LENGHT - mFirstFree - 1;
	}

	public int usedPoints() {
		return mFirstFree;
	}

	public float rightmostCoordinate() {
		return mRightmostX;
	}

	public boolean hasLeftVertexTo(float pX) {
		if(pX < 0)
			throw new ArrayIndexOutOfBoundsException("You must check for a vertices left to a positive coordinate. Sampled at: " + pX);
		for(int i = mFirstFree -1; i >= 0; i--)
			if(mPoints[i].x < pX)
				return true;
		return false;
	}

	//strictly left. It means that if you pass the exact coordinate of a vertex, this will NOT return that vertex coordinate
	public float leftVertexTo(float pX) {
		if(pX < 0)
			throw new ArrayIndexOutOfBoundsException("You must check for a vertices left to a positive coordinate. Sampled at: " + pX);
		for(int i = mFirstFree -1; i >= 0; i--)
			if(mPoints[i].x < pX)
				return mPoints[i].x;
		throw new IllegalStateException("There are no vertices at the left of " + pX + 
				", but this is impossible since there should always be a vertice left to 0 and you can not sample lower than 0.");
	}

	public List<Point> getPoints() {
		return mUnmodifiablePointList;
	}

	public float[] getMatrix(float[] pMatrix) {
		/*  0-->3  first row
		 *  4-->7  second row
		 *  8-->11 third row
		 * 12-->15 fourth row
		 */
		//TODO better document
		return getMatrix(pMatrix, 8, 4);
	}

	/**
	 * Copy the first points numerOfPoints from the left to the matrix, interleaving strife number of X coordinates, followed by the strife number of Y coordinates
	 * @param pMatrix The matrix to copy the points to
	 * @param numberOfPoints The number of points to copy
	 * @param strife The distance to keep between the X and Y coordinates of the same point. They are adjacent if 1 is used.
	 * @return
	 */
	public float[] getMatrix(float[] pMatrix, int numberOfPoints, int strife) {
		if(numberOfPoints <= 0 || strife <= 0) {
			throw new IllegalArgumentException("The number of points to be copied and the strife between X and Y coord must be greater than 0."
					+ "Number of points: " + numberOfPoints + ". Strife: " + strife);
		}
		if(pMatrix.length < numberOfPoints * 2) {
			throw new IllegalArgumentException("The passed matrix can not contain the (X,Y) coordinates of " + numberOfPoints + " poins. Matrix length: " + pMatrix.length);
		}
		for(int n = 0; n <= (numberOfPoints / strife); n++) {
			for(int i = 0; i < strife && strife * n + i < numberOfPoints; i++) {
				pMatrix[n * 2 * strife + i] = mPoints[n * strife + i].x;
				pMatrix[n * 2 * strife + i + strife] = mPoints[n * strife + i].y;
			}
		}
		return pMatrix;
	}

	@Override
	public float sample(float pX) {
		if(pX < 0  || pX > mRightmostX)
			throw new ArrayIndexOutOfBoundsException("You can not sample outside of the cavern. Cavern range: [ 0, " + mRightmostX + "]. Sampled at: " + pX);

		final int lastValid = mFirstFree - 1;
		int floor_index = 0;

		for(int j = 0; j < mFirstFree && mPoints[j].x <= pX; j++)
				floor_index = j;

		if(floor_index == lastValid)
			return mPoints[floor_index].y;
		else {
			final int ceil_index = floor_index + 1;
			float X1 = mPoints[floor_index].x;
			float Y1 = mPoints[floor_index].y;
			float X2 = mPoints[ceil_index].x;
			float Y2 = mPoints[ceil_index].y;
			return Interpolation.linearInterp(Y1, Y2, Interpolation.scurve3((pX - X1)/(X2 - X1)));
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for(int i = 0; i < mFirstFree; i++) {
			sb.append(mPoints[i].toString()).append(" ");
		}
		sb.append(" || ");
		for(int i = mFirstFree; i < ARRAY_LENGHT; i++) {
			sb.append("U").append(mPoints[i].toString()).append(" ");
		}
		sb.append("]");
		return sb.toString();
	}
}
