package com.makersf.games.nyanfly.cavern;

public class MatrixUtils {

	public static float[] scaleMatrix(float[] pMatrix, int lenghtOfRaw, float pXoffset, float pYoffset, float pXScale, float pYScale) {
		for(int i = 0; i < lenghtOfRaw; i++) {
			pMatrix[i + 0]             = pMatrix[i + 0]             * pXScale + pXoffset;//0-->3 first row
			pMatrix[i + lenghtOfRaw*1] = pMatrix[i + lenghtOfRaw*1] * pYScale + pYoffset;//4-->7 second row

			pMatrix[i + lenghtOfRaw*2] = pMatrix[i + lenghtOfRaw*2] * pXScale + pXoffset;//8-->11 third row
			pMatrix[i + lenghtOfRaw*3] = pMatrix[i + lenghtOfRaw*3] * pYScale + pYoffset;//12-->15 fourth row
		}
		return pMatrix;
	}
}
