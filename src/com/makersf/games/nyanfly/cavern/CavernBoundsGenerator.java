package com.makersf.games.nyanfly.cavern;

import java.util.List;

import it.francesco.utils.math.Interpolation;
import it.francesco.utils.math.MathUtilities;

import com.makersf.games.nyanfly.cavern.cave.CaveRappresentation;
import com.makersf.games.nyanfly.cavern.cave.Sampleable;
import com.makersf.games.nyanfly.cavern.obstacle.Obstacle;
import com.makersf.games.nyanfly.cavern.obstacle.ObstaclesRappresentation;
import com.makersf.games.nyanfly.cavern.random.IRandom;
import com.makersf.games.nyanfly.cavern.random.UniformRandom;

public class CavernBoundsGenerator implements CavernProvider {

	private static final float SECONDS_PER_MILLISECOND = 0.001f;
	private static final float MAX_UPDATE_STEP = 78 * SECONDS_PER_MILLISECOND;

	private static final int ARRAY_LENGHT = 8;
	// this indicates how many vertices should describe a cavern of size [0, 1]
	private static final int NUMBER_OF_VERTICES_PER_UNIT = 8;
	// once initialized, the cavern grants that there will be at least a vertices with X >= of this 
	private static final float GRANTED_RIGHTMOST_X = 1.5F;
	private static final IRandom RANDOM = new UniformRandom();//new NormalDistributedRandom();
	private static final float MINIMUM_HEIGHT_DISTANCE_ADJUSTMENT = 1.8f;
	private static final float MINIMUM_WIDTH_DISTANCE_ADJUSTEMENT = 1.3f;
	private static final float INVALID_NEXT_X_COORDINATE = -1;
	// ===========================================================
	// Fields
	// ===========================================================

	/*
	 * All the Coordinates are stored in relative [0,1] ranges!
	 * 0 = the most left(bottom) part of the screen
	 * 1 = the most right(up) part of the screen
	 */

	/*
	 * The list of anchor points that control the terrain.
	 * There should always be a poit with X < 0 and one with X > 1
	 */
	private CaveRappresentation mUpper;
	private float mNextUpperX;
	private CaveRappresentation mLower;
	private float mNextLowerX;

	private ObstaclesRappresentation mObstacles;
	private final float mRelativeVerticesStep;
	private IPlayerInformationsProvider mPlayer;

	//pass this to shaders in order to correclty show the "flow" of the maze
	private final TraveledDistance mDistance;
	private float mGrantedCavernLength;

	// ===========================================================
	// Constructors
	// ===========================================================

	public CavernBoundsGenerator(int pNumberOfVerticesPerScreen, final IPlayerInformationsProvider pMovingBody) {
		mRelativeVerticesStep = 1 / (float) pNumberOfVerticesPerScreen;
		mPlayer = pMovingBody;
		mDistance = new TraveledDistance(mRelativeVerticesStep);
		int arrayLength = (int) Math.ceil(GRANTED_RIGHTMOST_X * NUMBER_OF_VERTICES_PER_UNIT);
		//The default point must be always out of the possible range of the other points, otherwise may happen that it is used to interpolate values
		mLower = new CaveRappresentation(arrayLength, new Point(-200, 1.f / 3));
		mUpper = new CaveRappresentation(arrayLength, new Point(-200, 2.f / 3));
		mObstacles = new ObstaclesRappresentation();
		reset();
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	@Override
	public synchronized float getAlignedSpace() {
		return mDistance.getAlignedSpace();
	}

	@Override
	public synchronized float getUnalignedOffset() {
		return mDistance.getUnalignedSpace();
	}

	/**
	 * This is the structure of the matrix
	 * x1, x2, x3, x4
	 * y1, y2, y3, y4
	 * x5, x6, x7, x8
	 * y5, y6, y7, y8
	 *
	 * @param pMatrix
	 * @return
	 */
	@Override
	public synchronized float[] getLowerPointsMatrix(float[] pMatrix, float pWidth, float pHeight) {
		return MatrixUtils.scaleMatrix(mLower.getMatrix(pMatrix), ARRAY_LENGHT/2, 0, 0, pWidth, pHeight);
	}

	@Override
	public synchronized float[] getUpperPointsMatrix(float[] pMatrix, float pWidth, float pHeight) {
		return MatrixUtils.scaleMatrix(mUpper.getMatrix(pMatrix), ARRAY_LENGHT/2, 0, -pHeight, pWidth, pHeight);
	}

	@Override
	public synchronized List<Obstacle> getObstacles() {
		return mObstacles.getObstacles();
	}

	@Override
	public synchronized float sample(float pX, boolean pSampleUpper) {
		if(pX < 0  || pX > maxSampleable(pSampleUpper)) {
			throw new RuntimeException("You can not sample outside of the cavern. Sampled at: " + pX + ", the cavern reaches only " + mGrantedCavernLength);
		}
		if(pSampleUpper)
			return mUpper.sample(pX);
		else
			return mLower.sample(pX);
	}

	@Override
	public synchronized float sample(float pX, boolean pSampleUpper, float pWidth) {
		return sample(pX/pWidth, pSampleUpper);
	}

	@Override
	public float maxSampleable(boolean pSampleUpper) {
		if(pSampleUpper)
			return mUpper.rightmostCoordinate();
		else
			return mLower.rightmostCoordinate();
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public synchronized void reset() {
		// they must be initialized to the value for the first vertex
		mNextLowerX = 0;
		mNextUpperX = 0;
		mGrantedCavernLength = GRANTED_RIGHTMOST_X;
		mLower.clear();
		mUpper.clear();
		mObstacles.clear();
		stepUpdate(0); //populate the caverns
	}

	@Override
	public synchronized void onUpdate(float pSecondsElapsed) {
		/*
		 * Forces the stepUpdate function to be called with at most MAX_UPDATE_STEP as parameter.
		 * This prevents the errors during debugging in which the player would travel almost a full screen in a single update
		 * raising errors, and making debugging miserable
		 */
		float deltaTime = pSecondsElapsed;
		while(deltaTime > 0) {
			float stepTime = Math.min(deltaTime, MAX_UPDATE_STEP);
			stepUpdate(stepTime);
			deltaTime -= stepTime;
		}
	}

	public void stepUpdate(float pSecondsElapsed) {
		float coveredSpace = mPlayer.getPercentOfScreenTraveledPerSecond() * pSecondsElapsed;
		float alignedSpace = mDistance.addDistance(coveredSpace);
		mGrantedCavernLength = Math.max(GRANTED_RIGHTMOST_X, 1.f + mPlayer.getPercentOfScreenTraveledPerSecond() * MAX_UPDATE_STEP);

		if(alignedSpace != 0) {
			mUpper.offset(-alignedSpace, 0);
			mLower.offset(-alignedSpace, 0);
			mNextLowerX -= alignedSpace;
			mNextUpperX -= alignedSpace;
		}

		updateCave(mLower, false);
		updateCave(mUpper, true);

		boolean lowerNeedPoint = true;
		boolean upperNeedPoint = true;
		// genereate the points one per side. Not all one side at once, then the other side
		while( lowerNeedPoint || upperNeedPoint) {
			if(lowerNeedPoint = needsToAddPoint(mLower, false)) {
				populateCaveWithOnePoint(mLower, false);
			}
			if(upperNeedPoint = needsToAddPoint(mUpper, true)) {
				populateCaveWithOnePoint(mUpper, true);
			}
		}

		if(mLower.rightmostCoordinate() < mGrantedCavernLength || mUpper.rightmostCoordinate() < mGrantedCavernLength)
			throw new RuntimeException("The cavern is not spanning the granted distance.");
	}

	// ===========================================================
	// Methods
	// ===========================================================

	private void updateCave(CaveRappresentation pCave, boolean pIsUpper) {
		//the second one is out of screen, so the first one has no effect anymore on the displayed cave
		List<Point> points = pCave.getPoints();

		//only the first can be outside of the screen
		while(pCave.usedPoints() >= 2 && points.get(1).x < 0) {
			pCave.shiftLeft();
		}
		//it should enter here only after a reset
		if(pCave.usedPoints() == 0) {
			pCave.addPoint(generateFirstPoint(consumeNextX(pIsUpper), pIsUpper));
			precalculateNextX(pIsUpper);
		}
	}

	private boolean needsToAddPoint(CaveRappresentation pCave, boolean pIsUpper) {
		return pCave.hasUnusedPoints() && pCave.rightmostCoordinate() < mGrantedCavernLength;
	}

	private void populateCaveWithOnePoint(CaveRappresentation pCave, boolean pIsUpper) {
		pCave.addPoint(generatePoint(consumeNextX(pIsUpper), pIsUpper));
		// since the point was consumed, we need to generate it again
		precalculateNextX(pIsUpper);
	}

	private Point generateFirstPoint(float pX, boolean pIsUpper) {
		return new Point(pX, pIsUpper ? 3.f / 4 : 1.f / 4);
	}

	private void precalculateNextX(boolean pIsUpper) {
		CaveRappresentation current = pIsUpper ? mUpper : mLower;
		// the first parameter must be slightly more than 2.0f. Even if mVelocity*pUpdateTimeAt30FPS alone should be enough..
		float minX = current.rightmostCoordinate() + 1.f / (NUMBER_OF_VERTICES_PER_UNIT - 1) + mPlayer.getPercentOfScreenTraveledPerSecond() * MAX_UPDATE_STEP;
		float maxX = current.rightmostCoordinate() + 2 * 1.f / (NUMBER_OF_VERTICES_PER_UNIT - 1);
		if(minX > maxX) {
			// this may happen if the player is traveling "too" fast. An elegant solution to handle it might be needed
			throw new IllegalStateException("The min interval surpassed the max interval during X precalculation.");
		}

		float newX = (float) RANDOM.nextGaussian(minX, maxX);
		//make it a multiple of mVerticesStep
		newX = newX - (newX % mRelativeVerticesStep);

		if(pIsUpper) {
			if (mNextUpperX == INVALID_NEXT_X_COORDINATE) {
				mNextUpperX = newX;
			} else {
				throw new IllegalStateException("It has been tried to recalculate mNextUpperX while no one used the already present value yet.");
			}
		} else {
			if(mNextLowerX == INVALID_NEXT_X_COORDINATE) {
				mNextLowerX = newX;
			} else {
				throw new IllegalStateException("It has been tried to recalculate mNextLowerX while no one used the already present value yet.");
			}
		}
	}

	private float consumeNextX(boolean pIsUpper) {
		float ret;
		if(pIsUpper) {
			if(mNextUpperX == INVALID_NEXT_X_COORDINATE) {
				throw new IllegalStateException("You consumed more than a time the " + (pIsUpper ? "upper" : "lower") + " generated point");
			}
			ret = mNextUpperX;
			mNextUpperX = INVALID_NEXT_X_COORDINATE;
		} else {
			if(mNextUpperX == INVALID_NEXT_X_COORDINATE) {
				throw new IllegalStateException("You consumed more than a time the " + (pIsUpper ? "upper" : "lower") + " generated point");
			}
			ret = mNextLowerX;
			mNextLowerX = INVALID_NEXT_X_COORDINATE;
		}
		return ret;
	}
	/*
	 * Let's say we are adding a vertex to BOTTOM.
	 * If the player is at the TOP of the cave, and it starts to go up, it will reach a certain height when at pNewX.
	 * The new BOTTOM vertex can not be higher than this certain height minus the dimension of the player, otherwise he will
	 * not be able to pass.
	 * Since the cavern interpolate with a S-curve between the points, the player will not be able to instantly go up after the TOP vertex,
	 * so a little correction to this value is needed.
	 */
	private float limitImposedByPreviousVerticesOnOtherSide(CaveRappresentation pOppositeToCurrentCave, float pNewX, float pMinimumDistance, boolean pIsUpper) {

		
		float previousX = pOppositeToCurrentCave.leftVertexTo(pNewX);
		// we add MIN_VALUE so that the next call to leftVertexTo will return the same vertex returned by leftVertexTo(pNewX)
		previousX = Math.nextUp(previousX);
		// the initialization grants that the first max|min will always return the other value
		float limit = pIsUpper ? Float.NEGATIVE_INFINITY :  Float.POSITIVE_INFINITY;
		
		while(previousX >= 0 && pOppositeToCurrentCave.hasLeftVertexTo(previousX)) {
			previousX = pOppositeToCurrentCave.leftVertexTo(previousX);
			if(previousX >= 0) {
				float previousY = pOppositeToCurrentCave.sample(previousX);
				float time = (pNewX - previousX) / mPlayer.getPercentOfScreenTraveledPerSecond();
				if(pIsUpper) {
					float maxDownHeightDelta = mPlayer.getMinHeightDeltaIn(time);
					limit = Math.max(limit, previousY - maxDownHeightDelta + pMinimumDistance);
				} else {
					float maxUpHeightDelta = mPlayer.getMaxHeightDeltaIn(time);
					limit = Math.min(limit, previousY + maxUpHeightDelta - pMinimumDistance);
				}
			}
		}
		return MathUtilities.clamp(limit, 0, 1);
	}

	/*
	 * If there is a point on the opposite cavern between the pNewX and the previous point X, we need
	 * to check that this point will not intersect with the interpolated points of the current cave. 
	 * This method return the limit height that ensures that there will be at least minimumDistance
	 * between the opposite point and the interpolated points of the current cave, so that the player can pass throught
	 */
	private float limitToPreventInterpolatedPointsTooClose(CaveRappresentation pOppositeToCurrentCave, float pNewX, Point pPreviousPoint, float pMinimumDistance, boolean pIsUpper) {
		float otherLeftX = pOppositeToCurrentCave.leftVertexTo(pNewX);
		// we add MIN_VALUE so that the next call to leftVertexTo will return the same vertex returned by leftVertexTo(pNewX)
		otherLeftX = Math.nextUp(otherLeftX);
		// the initialization grants that the first max|min will always return the other value
		float limit = pIsUpper ? Float.NEGATIVE_INFINITY :  Float.POSITIVE_INFINITY;
		while(otherLeftX >= 0 && pOppositeToCurrentCave.hasLeftVertexTo(otherLeftX)) {
			otherLeftX = pOppositeToCurrentCave.leftVertexTo(otherLeftX);
			if(otherLeftX >= 0 && pPreviousPoint.x < otherLeftX && otherLeftX < pNewX) {
				float weight = Interpolation.scurve3((otherLeftX-pPreviousPoint.x)/(pNewX - pPreviousPoint.x));
				float otherY = pOppositeToCurrentCave.sample(otherLeftX);
				if(pIsUpper) {
					limit = Math.max(limit, Interpolation.rightLinearInterp(pPreviousPoint.y, otherY + pMinimumDistance, weight));
				} else {
					limit = Math.min(limit, Interpolation.rightLinearInterp(pPreviousPoint.y, otherY - pMinimumDistance, weight));
				}
			}
		}
		return MathUtilities.clamp(limit, 0, 1);
	}

	/*
	 * It may happen that a vertex is created so high/low, that the next one on the other side, due to limitToPreventInterpolatedPointsTooClose, should be
	 * higher than 1.0/0.0. This method checks for it and prevents the first vertex to by created so high/low.
	 */
	private float limitToPreventeForcedOutOfScreenNextOppositeVertex(CaveRappresentation pOppositeToCurrentCave, float pNewX, float pMinimumDistance, boolean pIsUpper) {
		float limit = pIsUpper ? Float.NEGATIVE_INFINITY :  Float.POSITIVE_INFINITY;
		float otherRightestX = pOppositeToCurrentCave.rightmostCoordinate();
		float nextOtherX = pIsUpper ? mNextLowerX : mNextUpperX;
		if(otherRightestX < pNewX && pNewX < nextOtherX) {
			// the top or the bottom of the screen
			float otherLimitValue = pIsUpper ? 0.0f : 1.0f;
			float otherY = pOppositeToCurrentCave.sample(otherRightestX);
			float weight = (pNewX - otherRightestX) / (nextOtherX - otherRightestX);
			float interpolatedValue = Interpolation.linearInterp(otherY, otherLimitValue, Interpolation.scurve3(weight));
			if (pIsUpper) {
				limit = interpolatedValue + pMinimumDistance;
			} else {
				limit = interpolatedValue - pMinimumDistance;
			}
		}
		return MathUtilities.clamp(limit, 0, 1);
	}

	private float limitForSpecificInterval(Sampleable pOppositeToCurrentCave, float pIntervallLeftLimit, float pIntervallRightLimit,
			Point pPreviousPoint, float pNewX, float pOffsetX, float pOffsetY, float pCurrentLimit, boolean pIsUpper) {
		float limit = pCurrentLimit;
		final int NUMBER_OF_STEPS = 10;
		final float step = (pIntervallRightLimit - pIntervallLeftLimit) / NUMBER_OF_STEPS;
		/* When RightLimit and LeftLimit are really close, but the absolute value is not small, step is smaller than
		 * nextUp, so nowX == nowX + step. To prevent this, we make sure nowX increases at least to nextUp
		 * TODO might be smart to find a faster way that calculating nextUp and max every iteration, as it is not always needed
		 */
		for(float nowX = pIntervallLeftLimit; nowX < pIntervallRightLimit; nowX = Math.max(Math.nextUp(nowX), nowX + step)) {
			/* Why Math.nextUp? The reason is that from how the left interval is calculated it happens really
			 * often that it coincides with pOffsetX - pPreviousPoint.x, thus weight is 0.
			 * But since a weight = 0 is an illegal argument for rightLinearInterp, we increase it by a little bit
			 * This will not change the result of the operation, but just prevent this "coincidence"
			 */
			float nowOppositeY = pOppositeToCurrentCave.sample(nowX);
			float nowCurrentY = nowOppositeY + pOffsetY;
			// calculate the weight of the nowX point offsetted by the width of the player
			// so that we check that a rectangule can pass
			float currentWeight = (nowX + pOffsetX - pPreviousPoint.x) / (pNewX - pPreviousPoint.x);
			if(currentWeight <= 0)
				continue;
			float height = Interpolation.rightLinearInterp(pPreviousPoint.y, nowCurrentY, currentWeight);
			if (pIsUpper) {
				limit = Math.max(limit, height);
			} else {
				limit = Math.min(limit, height);
			}
		}
		return limit;
	}

	private float limitToAvoidChockesAlongAllTheSCurve(CaveRappresentation pOppositeToCurrentCave, float pNewX, Point pPreviousPoint, float pPlayerHeight, float pPlaywerWidth, boolean pIsUpper) {
		float limit = pIsUpper ? Float.NEGATIVE_INFINITY :  Float.POSITIVE_INFINITY;
		float otherRightX = pOppositeToCurrentCave.rightmostCoordinate();
		float otherFutureX = pIsUpper ? mNextLowerX : mNextUpperX;
		float offsetY = pIsUpper ? pPlayerHeight : -pPlayerHeight;

		if(otherRightX < pNewX && pNewX < otherFutureX) {
			// the limit at which the opposite cave can be
			// NOTE: we are gathering info on the opposite cave! You should expect pIsUpper ? 1:0, but we are checking for the opposite cave!
			float otherRightFutureLimitY = pIsUpper ? 0 : 1;
			// since we are imposing the limit height, the upper cave is always raising and the bottom cave is always descending
			float offsetX = pPlaywerWidth;
			float left = otherRightX;
			float right = Math.min(otherFutureX, pNewX - offsetX);
			float otherRightY = pOppositeToCurrentCave.sample(otherRightX);
			Sampleable fakeCavern = new NotYetCreatedIntervall(otherRightX, otherRightY, otherFutureX, otherRightFutureLimitY);
			limit = limitForSpecificInterval(fakeCavern, left, right, pPreviousPoint, pNewX, offsetX, offsetY, limit, pIsUpper);
		}
		// NOTE: REMEMBER THAT THE IF CODE IS A SPECIFIC CASE FOR THE WHILE ONE. IF YOU CHANGE ONE, YOU SHOULD PROBABLY CHANGE THE OTHER TOO!!!!
		float otherLeftX = pOppositeToCurrentCave.rightmostCoordinate();
		while(otherLeftX >= 0 && pOppositeToCurrentCave.hasLeftVertexTo(otherLeftX) && otherRightX >= pPreviousPoint.x) {
			otherRightX = otherLeftX;
			otherLeftX = pOppositeToCurrentCave.leftVertexTo(otherLeftX);
			boolean isOppositeRaising = pOppositeToCurrentCave.sample(otherRightX) - pOppositeToCurrentCave.sample(otherLeftX) > 0;
			float offsetX = isOppositeRaising != pIsUpper ? pPlaywerWidth : -pPlaywerWidth;
			float left = Math.max(otherLeftX, pPreviousPoint.x - offsetX);
			float right = Math.min(otherRightX, pNewX - offsetX);
			if(right < left)
				continue;
			else
				limit = limitForSpecificInterval(pOppositeToCurrentCave, left, right, pPreviousPoint, pNewX, offsetX, offsetY, limit, pIsUpper);
		}
		return MathUtilities.clamp(limit, 0, 1);
	}

	/*
	 * There should always be at least minimumDistance between the new point and the sampled point at the same X on
	 * the other cave. This method check the limit that make this condition true.
	 */
	private float limitToAvoidChocke(CaveRappresentation pOppositeToCurrentCave, float pNewX, float pMinimumDistance, boolean pIsUpper) {
		float limit = pIsUpper ? Float.NEGATIVE_INFINITY :  Float.POSITIVE_INFINITY;
		if (pNewX >= 0 && pOppositeToCurrentCave.rightmostCoordinate() >= pNewX) {
			float otherY = pOppositeToCurrentCave.sample(pNewX);
			if (pIsUpper) {
				limit = otherY + pMinimumDistance;
			} else {
				limit = otherY - pMinimumDistance;
			}
		}
		return MathUtilities.clamp(limit, 0, 1);
	}

/* //I think this method is too forgiving for the player: he don't need to plan ahead where to go
	private float limitToGrantThePlayerWillBeAbleToPassWhereverHeIsWhenAtPreviousVertex(CaveRappresentation pCurrentCave, float pNewX, boolean pIsUpper) {
		float previousX = pCurrentCave.leftVertexTo(pNewX);
		float previousY = pCurrentCave.sample(previousX);
		float time = (pNewX - previousX) / mPlayer.getPercentOfScreenTraveledPerSecond();
		if(pIsUpper) {
			float maxDownHeightDelta = mPlayer.getMinHeightDeltaIn(time);
			return previousY - maxDownHeightDelta;
		} else {
			float maxUpHeightDelta = mPlayer.getMaxHeightDeltaIn(time);
			return previousY + maxUpHeightDelta;
		}
	}
*/

	private Point generatePoint(float pNewX, boolean pIsUpper) {

		CaveRappresentation other = pIsUpper ? mLower : mUpper;
		CaveRappresentation current = pIsUpper ? mUpper : mLower;

		float thisPreviousX = current.rightmostCoordinate();

		//determine the new Y coord
		float thisPreviousY = current.sample(thisPreviousX);

		//TODO querry the blocks that eventually are in the screen
		//TODO almost grant that the player can pass
		float newUpperY;
		float newLowerY;
		final float minimumDistance = MINIMUM_HEIGHT_DISTANCE_ADJUSTMENT * mPlayer.getHeight();
		final float playerWidth = MINIMUM_WIDTH_DISTANCE_ADJUSTEMENT * mPlayer.getWidth();

		float limitOtherSide = limitImposedByPreviousVerticesOnOtherSide(other, pNewX, minimumDistance, pIsUpper);
		Point temp = new Point(thisPreviousX, thisPreviousY);
		float limitInterpolation = limitToPreventInterpolatedPointsTooClose(other, pNewX, temp, minimumDistance, pIsUpper);
		// we need to check both with the offset of playerWidht and 0 because we must grant that there is at least 
		// minimum distance between the two curves over the 2 sides of the rectangle
		float limitToPreventChocksOnMiddleSCurve = limitToAvoidChockesAlongAllTheSCurve(other, pNewX, temp, minimumDistance, playerWidth, pIsUpper);
		float limitToGrantMinimumDistanceVertically = limitToAvoidChockesAlongAllTheSCurve(other, pNewX, temp, minimumDistance, 0, pIsUpper);
		float limitOutOfScreenInterpolation = limitToPreventeForcedOutOfScreenNextOppositeVertex(other, pNewX, minimumDistance, pIsUpper);
		float limitChocke =  limitToAvoidChocke(other, pNewX, minimumDistance, pIsUpper);
		
		if(pIsUpper) {
			float max1 = Math.max(limitOtherSide, limitInterpolation);
			float max2 = Math.max(limitToPreventChocksOnMiddleSCurve, limitOutOfScreenInterpolation);
			float minimumY = Math.max(Math.max(max1, limitToGrantMinimumDistanceVertically), Math.max(max2, limitChocke));
			newLowerY = MathUtilities.clamp(minimumY, 0.1f, 1);
			newUpperY = 1.f;
		} else {
			float min1 = Math.min(limitOtherSide, limitInterpolation);
			float min2 = Math.min(limitToPreventChocksOnMiddleSCurve, limitOutOfScreenInterpolation);
			float maximumY = Math.min(Math.min(min1, limitToGrantMinimumDistanceVertically), Math.min(min2, limitChocke));
			newUpperY = MathUtilities.clamp(maximumY, 0 , 0.9f);
			newLowerY = 0.f;
		}
		float newY = (float) RANDOM.nextGaussian(newLowerY, newUpperY);

		return new Point(pNewX, newY);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("IMovingBody: %s. Traveled distance: %f + %f", mPlayer.toString() , mDistance.getAlignedSpace(), mDistance.getUnalignedSpace())).append("\n");
		sb.append(mUpper).append("\n").append(mLower).append("\n");
		sb.append("Next upper X: ").append(mNextUpperX).append(", lower X: ").append(mNextLowerX);
		return sb.toString();
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

	private class NotYetCreatedIntervall implements Sampleable {

		private final Point mLeftPoint;
		private final Point mRightPoint;

		public NotYetCreatedIntervall(float pLeftX, float pLeftY, float pRightX, float pRightY) {
			mLeftPoint = new Point(pLeftX, pLeftY);
			mRightPoint = new Point(pRightX, pRightY);
		}

		@Override
		public float sample(float pX) {
			float weight = (pX - mLeftPoint.x) / (mRightPoint.x - mLeftPoint.x);
			float s3 = Interpolation.scurve3(weight);
			return Interpolation.linearInterp(mLeftPoint.y, mRightPoint.y, s3);
		}
		
	}
}
