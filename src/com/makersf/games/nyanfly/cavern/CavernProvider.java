package com.makersf.games.nyanfly.cavern;

import java.util.List;

import com.makersf.games.nyanfly.cavern.obstacle.Obstacle;

public interface CavernProvider {

	public float getAlignedSpace();
	public float getUnalignedOffset();

	public void onUpdate(float pSecondsElapsed);
	public void reset();

	public float[] getLowerPointsMatrix(float[] pMatrix, float pWidth, float pHeight);
	public float[] getUpperPointsMatrix(float[] pMatrix, float pWidth, float pHeight);
	public List<Obstacle> getObstacles();

	public float sample(float pX, boolean pSampleUpper);
	public float sample(float pX, boolean pSampleUpper, float pWidth);
	public float maxSampleable(boolean pSampleUpper);
}
