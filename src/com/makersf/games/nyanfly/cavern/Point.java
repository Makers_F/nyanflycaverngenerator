package com.makersf.games.nyanfly.cavern;

import java.util.Comparator;

public class Point {

	public float x;
	public float y;

	public Point() {
	}

	public Point(float pX, float pY) {
		x = pX;
		y = pY;
	}

	public Point reset() {
		x = 0;
		y = 0;
		return this;
	}

	public Point offset(float pX, float pY) {
		x += pX;
		y += pY;
		return this;
	}

	public Point set(Point pPoint) {
		x = pPoint.x;
		y = pPoint.y;
		return this;
	}

	public Point set(float pX, float pY) {
		x = pX;
		y = pY;
		return this;
	}

	public Point setX(float pX) {
		x = pX;
		return this;
	}

	public Point setY(float pY) {
		y = pY;
		return this;
	}

	@Override
	public String toString() {
		return "Point( " + x + ", " + y + ")";
	}

	public static class PointComparator implements Comparator<Point> {

		@Override
		public int compare(Point arg0, Point arg1) {
			if (arg0.x < arg1.x)
				return -1;
			else if (arg0.x > arg1.x)
				return 1;
			else
				return 0;
		}
	}
}
