package com.makersf.games.nyanfly.cavern.random;

import it.francesco.utils.math.random.CloneableRandom;

public class UniformRandom implements IRandom {

	private CloneableRandom mRandom;

	public UniformRandom() {
		mRandom = new CloneableRandom();
	}

	private UniformRandom(CloneableRandom pRandom) {
		mRandom = pRandom;
	}

	@Override
	public double nextGaussian(double pLowerLimit, double pUpperLimit) {
		//TODO Possibily huge bug
		return pLowerLimit + mRandom.nextFloat() * (pUpperLimit - pLowerLimit);
	}

	@Override
	public UniformRandom clone() {
		return new UniformRandom(mRandom.clone());
	}

}
