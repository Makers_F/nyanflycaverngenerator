package com.makersf.games.nyanfly.cavern.random;

import it.francesco.utils.math.MathUtilities;
import it.francesco.utils.math.random.CloneableRandom;

public class NormalDistributedRandom implements IRandom {

	private static final double DEFAULT_DIFFICULTY_CORRECTION = 0.2;
	private CloneableRandom mRandom;
	private double mDifficultyCorrection;

	public NormalDistributedRandom() {
		this(DEFAULT_DIFFICULTY_CORRECTION);
	}

	/**
	 * 
	 * @param pDifficultyCorrection Lower is easier. It must be >= 0
	 */
	public NormalDistributedRandom(double pDifficultyCorrection) {
		mRandom = new CloneableRandom();
		mDifficultyCorrection = Math.max(pDifficultyCorrection, 0);
	}

	public double nextGaussian() {
		return mRandom.nextGaussian(); // range [-inf, +inf]
	}

	@Override
	public double nextGaussian(double pLowerLimit, double pUpperLimit) {
		double mean = (pLowerLimit + pUpperLimit ) / 2;
		double distanceFromLimit = (pUpperLimit - pLowerLimit ) / 2;
		double nextNormal = mean + nextGaussian() * distanceFromLimit * mDifficultyCorrection;
		return MathUtilities.clamp(nextNormal, pLowerLimit, pUpperLimit);
	}

	@Override
	public NormalDistributedRandom clone() {
		NormalDistributedRandom newNormal = new NormalDistributedRandom(mDifficultyCorrection);
		newNormal.mRandom = mRandom.clone();
		return newNormal;
	}
}
