package com.makersf.games.nyanfly.cavern.random;

public interface IRandom extends Cloneable {
	public double nextGaussian(double min, double max);
	public IRandom clone();
}
