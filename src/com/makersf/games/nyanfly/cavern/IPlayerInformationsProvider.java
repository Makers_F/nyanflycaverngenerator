package com.makersf.games.nyanfly.cavern;

public interface IPlayerInformationsProvider {

	public float getWidth();
	public float getHeight();
	/**
	 *
	 * @return The current velocity on the X axis. It must be width independent
	 */
	public float getPercentOfScreenTraveledPerSecond();
	/**
	 * This methods return the maximum up variation in height the player can make in pSeconds seconds in the worst possible condition.
	 * This method should be pure: it ignores the current state of the player and return always the same value for the same pSeconds
	 * @param pSeconds The seconds during which the player can move
	 * @return The foreseen maximum variation in height. Must be positive and in percentage of the screen
	 */
	public float getMaxHeightDeltaIn(float pSeconds);
	/**
	 * This methods return the maximum down variation in height the player can make in pSeconds seconds in the worst possible condition.
	 * This method should be pure: it ignores the current state of the player and return always the same value for the same pSeconds
	 * @param pSeconds The seconds during which the player can move
	 * @return The foreseen maximum variation in height. Must be positive and in percentage of the screen
	 */
	public float getMinHeightDeltaIn(float pSeconds);
}
