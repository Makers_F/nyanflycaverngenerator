package com.makersf.games.nyanfly.cavern.obstacle;

import java.util.Comparator;

public class Obstacle {

	public float x;
	public float y;
	public ObstacleType type;
	public int ID;

	public Obstacle() {
		ID = System.identityHashCode(this);
	}

	public Obstacle(float pX, float pY, ObstacleType pType) {
		x = pX;
		y = pY;
		type = pType;
	}

	public Obstacle offset(float pX, float pY) {
		x += pX;
		y += pY;
		return this;
	}

	public Obstacle set(Obstacle pObstacle) {
		x = pObstacle.x;
		y = pObstacle.y;
		return this;
	}

	public Obstacle set(float pX, float pY) {
		x = pX;
		y = pY;
		return this;
	}

	public Obstacle set(float pX, float pY, ObstacleType pType) {
		x = pX;
		y = pY;
		type = pType;
		return this;
	}

	public Obstacle setX(float pX) {
		x = pX;
		return this;
	}

	public Obstacle setY(float pY) {
		y = pY;
		return this;
	}

	public Obstacle setType(ObstacleType pType) {
		type = pType;
		return this;
	}

	/**
	 *
	 * @param pX the X coordinate to test. If Nan is passed it doesn't check for it
	 * @param pY the Y coordinate to test. If Nan is passed it doesn't check for it
	 * @return Whether the point is contained or not
	 */
	public boolean contains(float pX, float pY) {
		return !((pX - x) > type.width) && !((pY - y) > type.height);
	}

	@Override
	public String toString() {
		return "Obstacle( " + x + ", " + y + ", " + type + ")";
	}

	public static class ObstacleComparator implements Comparator<Obstacle> {

		@Override
		public int compare(Obstacle lhs, Obstacle rhs) {
			if(lhs.x < rhs.x)
				return -1;
			else if (lhs.x > rhs.x)
				return +1;
			else
				return 0;
		}
	};

	public static enum ObstacleType {
		BIG(0.02f, 0.04f),
		SMALL(0.015f, 0.02f);

		public final float width;
		public final float height;

		ObstacleType(float pWidth, float pHeight) {
			width = pWidth;
			height = pHeight;
		}

		public static ObstacleType max(ObstacleType arg0, ObstacleType arg1) {
			return arg0 == BIG || arg1 == BIG ? BIG : SMALL;
		}
	}
}
