package com.makersf.games.nyanfly.cavern.obstacle;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class ObstaclesRappresentation {

	private static final Comparator<Obstacle> comparator = new Obstacle.ObstacleComparator();

	private final LinkedList<Obstacle> mObstacles = new LinkedList<Obstacle>();
	private final List<Obstacle> mUnmodifiableObstacles = Collections.unmodifiableList(mObstacles);

	public void clear() {
		mObstacles.clear();
	}

	public void offset(float pXOffset, float pYOffset) {
		for(Obstacle o : mObstacles) {
			o.offset(pXOffset, pYOffset);
		}
	}

	public void shiftLeft() {
		mObstacles.poll();
	}

	public boolean addObstacle(Obstacle pNewObstacle) {
		for(Obstacle o : mObstacles)
			if(o.contains(pNewObstacle.x, pNewObstacle.y))
				//return false;
				throw new ObstacleOverlapsException(pNewObstacle, o);
		mObstacles.add(pNewObstacle);
		Collections.sort(mObstacles, comparator);
		return true;
	}

	public List<Obstacle> getObstacles() {
		return mUnmodifiableObstacles;
	}

	public List<Float> sample(float pX) {
		List<Float> points = new LinkedList<Float>();
		for(Obstacle o : mObstacles) {
			if(o.contains(pX, Float.NaN)) {
				points.add(o.y);
				points.add(o.y + o.type.height);
			}
		}
		return points;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(64);
		sb.append("[ ");
		for(Obstacle o : mObstacles) {
			sb.append(o.toString()).append(" ");
		}
		sb.append("]");
		return sb.toString();
	}

	public static class ObstacleOverlapsException extends RuntimeException {
		private static final long serialVersionUID = 2117724368049651443L;
		public ObstacleOverlapsException(Obstacle overlapping, Obstacle old) {
			super("The new obstacle " + overlapping.toString() + " overlaps with " + old.toString());
		}
	}
}
