package com.makersf.games.nyanfly.cavern;

class TraveledDistance {
	private final float mVerticesStep;
	private float mUnaligned;
	private float mAligned;

	public TraveledDistance(float pVerticesStep) {
		this.mVerticesStep = pVerticesStep;
	}

	/**
	 *
	 * @param pCoveredSpace The space covered
	 * @return The part of covered space that is alligned with the vertiecs step
	 */
	public float addDistance(float pCoveredSpace) {
		float totalPartialSpace = mUnaligned + pCoveredSpace;
		mUnaligned = totalPartialSpace % mVerticesStep;
		mAligned += totalPartialSpace - mUnaligned;
		return totalPartialSpace - mUnaligned;
	}

	public float getUnalignedSpace() {
		return mUnaligned;
	}

	public float getAlignedSpace() {
		return mAligned;
	}

	@Override
	public String toString() {
		return "Alligned: " + mAligned + ", Unaligned: " + mUnaligned + ". Vertice step: " + mVerticesStep;
	}
}